from django.urls import path
from . import views
from . import models

app_name = 'story4'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('about2/', views.about2, name='about2'),
    path('contact/', views.contact, name='contact'),
    path('schedule/', views.schedule, name='schedule'),
    path('createsched/', views.createsched, name='createsched'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),
]


