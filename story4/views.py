from django.shortcuts import render, redirect
from .models import Schedule
from . import forms


# Create your views here.
def home(request):
    return render(request, "home.html")

def about(request):
    return render(request, "about.html")

def about2(request):
    return render(request, "about2.html")

def contact(request):
    return render(request, "contact.html")

# def jadwal(request):
#     jadwalnya = Jadwal.objects.all().values()
    
#     response = {'jadwal': jadwalnya}
#     return render(request,'jadwal.html',response)

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def createsched(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story4:schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'createsched.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")





